const cheerio = require('cheerio');
const fs = require('fs');
const https = require('https');
const Twitter = require('twitter');

const secrets = JSON.parse(fs.readFileSync('.twitter-secrets.json'));
const client = new Twitter(secrets);

function fetch(url, base64) {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      if (res.statusCode !== 200) {
        return reject(new Error(`${url} returned unexpected status code: ${res.statusCode}`));
      }

      let data = [];
      res.on('data', (chunk) => {
        data.push(chunk)
      });

      res.on('end', () => {
        if (base64) {
          const buffer = Buffer.concat(data);
          resolve(buffer.toString('base64'));
        } else {
          resolve(data.join(''));
        }
      });
    }).on('error', reject);
  });
}

function parseFirstItem(feedXml) {
  const $ = cheerio.load(feedXml, {
    xmlMode: true
  });
  const $firstItem = $('item').first();
  const title = $firstItem.children('title').first().text();
  const comicUrl = $firstItem.children('link').first().text();
  const comicDate = new Date($firstItem.children('pubDate').first().text());
  const $description = cheerio.load($firstItem.children('description').text(), {
    xmlMode: true
  });
  const $comicImage = $description('img.comic').first();
  const imageUrl = $comicImage.attr('src');
  const mouseover = $comicImage.attr('title').substring((title + ' -- ').length);
  return { title, comicUrl, comicDate, imageUrl, mouseover };
}

function ignoreOldComics(item) {
  const lastTweet = JSON.parse(fs.readFileSync('.last-tweet.json'));

  if (lastTweet.comicDate === item.comicDate.toISOString()) {
    throw new Error(`${lastTweet.url} exists already! (comic published ${lastTweet.comicDate})`);
  }

  return item;
}

function uploadImage(image) {
  return client.post('media/upload', {media_data: image})
    .then((media) => media.media_id_string);
}

function sendTweet(item) {
  const status = `${item.title}\n\n${item.mouseover}\n\n${item.comicUrl}\n`;
  const media_ids = item.media_id_string;
  return client.post('statuses/update', { status, media_ids})
    .then((tweet) => {
      return {
        comicDate: item.comicDate,
        url: `https://twitter.com/bastianmelnyk/status/${tweet.id_str}`
      };
    });
}

fetch('https://www.fonflatter.de/feed/')
  .then(parseFirstItem)
  .then(ignoreOldComics)
  .then((item) => {
    return fetch(item.imageUrl, true)
      .then(uploadImage)
      .then((media_id_string) => {
        item.media_id_string = media_id_string;
        return item;
      });
  })
  .then(sendTweet)
  .then((lastTweet) => {
    fs.writeFileSync('.last-tweet.json', JSON.stringify(lastTweet));
    console.log(lastTweet)
  })
  .catch(console.error);
